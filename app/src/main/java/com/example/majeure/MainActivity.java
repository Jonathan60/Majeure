package com.example.majeure;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.CustomZoomButtonsController;
import org.osmdroid.views.MapView;

import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.List;


import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.ContentValues.TAG;

public class MainActivity extends Activity implements View.OnClickListener {
    MapView map = null;
    MyLocationNewOverlay location;
    ScaleBarOverlay scale;

    WifiManager wifi;
    Button buttonScan;
    Button buttonSend;

    int size = 0;
    List<ScanResult> results;

    long timestamp;
    double latitude;
    double longitude;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize OpenStreetMap configuration
        Context context = getApplicationContext();
        Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context));

        // create the map
        setContentView(R.layout.activity_main);
        map = findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        // allows zooming
        map.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.ALWAYS);
        map.setMultiTouchControls(true);

        // by default, begins the on the port Vannes
        IMapController mapController = map.getController();
        mapController.setZoom(15.0);
        GeoPoint startPoint = new GeoPoint(47.6541, -2.758146);
        mapController.setCenter(startPoint);

        // allows geolocation
        location = new MyLocationNewOverlay(new GpsMyLocationProvider(context), map);
        this.location.enableMyLocation();
        map.getOverlays().add(this.location);


        // add the scale
        final DisplayMetrics dm = context.getResources().getDisplayMetrics();
        scale = new ScaleBarOverlay(map);
        scale.setCentred(true);
        scale.setScaleBarOffset(dm.widthPixels / 2, 10);
        map.getOverlays().add(this.scale);

        // create the scan button
        buttonScan = findViewById(R.id.buttonScan);
        buttonScan.setOnClickListener(this);

        buttonSend = findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(this);

        // enabling WiFi
        wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

    }


    public void onClick(View view) {
        // execute scanWifiNetworks when the button is pressed
        switch (view.getId()){
            case R.id.buttonScan:
                isWifiEnabled(wifi);
                scanWifiNetworks();
                break;
            case R.id.buttonSend:
                sendToServer();
                break;
            default:
                break;
        }
    }

        public void onResume() {
        super.onResume();
        // reloads the map (for geolocation)
        map.onResume();
    }

    public void onPause() {
        super.onPause();
        // reloads OSM configuration (for geolocation)
        map.onPause();
    }


    //check if WiFi is enabled
    public void isWifiEnabled(WifiManager wifi) {
        if(!wifi.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), "Enabling WiFi...", Toast.LENGTH_LONG).show();
            wifi.setWifiEnabled(true);
        }

        if(!wifi.isWifiEnabled()){
            Toast.makeText(getApplicationContext(), "Please deactivate connection sharing and enable WiFi", Toast.LENGTH_LONG).show();
        }
    }


    private void scanWifiNetworks() {

        LocationManager lm = (LocationManager) getSystemService(Context. LOCATION_SERVICE) ;
        boolean gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // check is location is enabled : if it is, start scanning
        if(gps_enabled) {
            registerReceiver(mWiFiBroadcastReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)); // with the broadcast receiver,
            wifi.startScan();                                                                                      // scans APs all around
            Toast.makeText(this, "Scan in progress", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Please enable location", Toast.LENGTH_SHORT).show();
        }
    }


    BroadcastReceiver mWiFiBroadcastReceiver = new BroadcastReceiver() {    // creates a broadcast receiver
        @Override
        public void onReceive(Context c, Intent intent) {

            results = wifi.getScanResults();    // get the SSIDs of the APs
            size = results.size();
            Toast.makeText(getApplicationContext(), size + " APs have been found", Toast.LENGTH_SHORT).show();

            unregisterReceiver(this);

            // get the timestamp, the latitude and the longitude when the button is pressed
            getTimestamp();
            getLatitude();
            getLongitude();

            // for all APs, get the MAC address, the power and the SSID when the button is pressed and writes them into a json local file
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(c.openFileOutput("data.json", Context.MODE_APPEND));
                Log.d(TAG, "Writing in file");

                while (size > 0) {

                    size--;

                    String mac = results.get(size).BSSID;
                    int power = results.get(size).level;
                    String ssid = results.get(size).SSID;

                    JSONObject object = new JSONObject();

                    // put all the values in json
                    object.put("timestamp", timestamp);
                    object.put("type", "access_point");
                    object.put("mac", mac);
                    object.put("power", power);
                    object.put("ssid", ssid);
                    object.put("latitude", latitude);
                    object.put("longitude", longitude);

                    String json = String.valueOf(object);

                    Log.d(TAG, json);
                    outputStreamWriter.write(json + "\n");
                }
                outputStreamWriter.close();

            } catch (Exception e) {
                Log.w(TAG, "Exception : " + e);
            }
        }
    };


    // get the last known location if exists
    private Location getLastKnownLocation() {
        Location l = null;
        LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                l = mLocationManager.getLastKnownLocation(provider);
            }
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public long getTimestamp(){
        timestamp = System.currentTimeMillis() / 1000;
        return timestamp;
    }

    public double getLatitude() {
        latitude = (Math.round(getLastKnownLocation().getLatitude() * 100000)) / 100000.0;
        return latitude;
    }
    public double getLongitude() {
        longitude = (Math.round(getLastKnownLocation().getLongitude() * 100000)) / 100000.0;
        return longitude;
    }

    // sends the data from the json local file to a distant server
    public void sendToServer() {
        try {
            // puts the network thread into the main thread (otherwise there is a NetworkOnMainThread exception)
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);


            File file = new File(getFilesDir().getAbsolutePath() + "/data.json");
            if (file.exists()) {

                BufferedReader br = new BufferedReader(new FileReader(getFilesDir().getAbsolutePath() + "/data.json"));
                String line;
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("application/json");

                Toast.makeText(this, "Sending data", Toast.LENGTH_SHORT).show();

                String urlServer = "http://192.168.43.6:8000/api/save";

                while ((line = br.readLine()) != null) {
                    RequestBody body = RequestBody.create(mediaType, line);

                    Request request = new Request.Builder()
                            .url(urlServer)      // url to be replaced
                            .method("POST", body)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("api_key", "MAJEUREPROJECT20192020TOKENOSINT")
                            .addHeader("Authorization", "Basic Og==")
                            .build();
                    Response response = client.newCall(request).execute();

                    Log.d(TAG, line);
                }
                Toast.makeText(this, "Data has been sent to " + urlServer, Toast.LENGTH_SHORT).show();
                br.close();
                file.delete();
            }
            else{
                Toast.makeText(this, "Please make a scan first", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Log.w(TAG, "Exception : " + e);
            Toast.makeText(this, "Data has not been sent", Toast.LENGTH_SHORT).show();

        }
    }
}


