## <u>Application Android</u> : Guide de déploiement

### Téléchargement du .apk

Pour télécharger les paquets de l'application Android, veuillez aller sur votre smartphone et cliquer sur [ce lien](https://gitlab.com/Jonathan60/Majeure/blob/master/Majeure.apk) afin de récupérer le .apk.

### Installation de l'application

#### Appareils Samsung

Pour installer l'application, allez dans `Mes fichiers`, `Fichiers installés`, `Download`, `Majeure.apk`
Appuyez sur `Installer` puis sur `Ok` à la fin de l'installation.

Il faut ensuite accorder les permissions de stockage et de GPS pour l'application (pour une raison obscure, l'appli ne demande pas par elle-même, il faut l'activer à la main)
Allez dans les paramètres du téléphone. Dans `Applications`, sélectionnez `Majeure` puis `Autorisations`. Autorisez la position et le stockage.

### Utilisation de l'application

Lors de l'ouverture de l'application :
* si vous voulez seulement vous géolocaliser, **activez** la `géolocalisation` ;
* si vous voulez scanner les points d'accès autour de vous, veuillez **activer** la `géolocalisation` et le `WiFi` et **désactiver** le `partage de connexion` s'il est activé.
* si vous voulez envoyer les données à un serveur distant (par défaut en local 192.168.43.225:8000/api/save), veuillez activer le `WiFi` ou les `données mobiles`.


#### Modification d'URL pour le serveur

Malheureusement, le code ne comporte pas de champ pour changer d'URL.
Si jamais vous devez changer d'URL, vous devrez **télécharger Android Studio** et modifier le code en conséquence :
* Clonez avec la commande `git clone https://gitlab.com/Jonathan60/Majeure/blob/master/app/src/main/java/com/example/majeure/MainActivity.java
* Ouvrez avec Android Studio le fichier app/src/main/java/com/example/majeure/MainActivity.java
* Cherchez avec CTRL + F la chaîne de caractère "urlServer"
* Modifiez le champ de l'adresse IP
* Buildez un .apk en allant sur `Build` - `Build Bundle(s) / APK(s)` - `Build APK(s)`
* Cliquez sur `locate` dans le fenêtre qui apparait
* Renommez `app-debug.apk` en `majeure.apk`
* Connectez votre smartphone à votre PC
* Déplacez `majeure.apk` dans [Votre smarpthone]/Download
* Pour installer l'application, veuillez lire la section `Installation de l'application`